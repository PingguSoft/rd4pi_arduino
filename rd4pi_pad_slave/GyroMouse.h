/*
 This project is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is derived from deviationTx project for Arduino.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 see <http://www.gnu.org/licenses/>
*/

#ifndef _GYRO_MOUSE_H_
#define _GYRO_MOUSE_H_
#include <Arduino.h>
#include <stdarg.h>
#include "utils.h"
#include "src/MPU6050/helper_3dmath.h"
#define MPU6050_INCLUDE_DMP_MOTIONAPPS20
#include "src/MPU6050/MPU6050.h"

/*
*****************************************************************************************
* MACROS
*****************************************************************************************
*/



class GyroMouse
{
public:
    void setup(void);
    u8   process(long ts, s8 *mx, s8 *my, s8 *mz);

private:
    s8    cut(int val);
    float delta(float val);
    static void  dmpDataReady(void);

    //
    static GyroMouse *instance;
    
    //
    MPU6050       mpu;
    bool          isBlink        = false;
    volatile bool isMpuInterrupt = false;    // indicates whether MPU interrupt pin has gone high
    //
    // MPU control/status vars
    bool          isDmpReady   = false;      // set true if DMP init was successful
    u8            mpuIntStatus;              // holds actual interrupt status byte from MPU
    u8            devStatus;                 // return status after each device operation (0 = success, !0 = error)
    u16           packetSize;                // expected DMP packet size (default is 42 bytes)
    u16           fifoCount;                 // count of all bytes currently in FIFO
    u8            fifoBuffer[64];            // FIFO storage buffer
    //
    // orientation/motion vars
    Quaternion    q;                         // [w, x, y, z]         quaternion container
    VectorFloat   gravity;                   // [x, y, z]            gravity vector
    float         ypr[3];                    // [yaw, pitch, roll]   yaw/pitch/roll container and gravity vector
    //
    //
    float         fYaw;
    float         fPit;
    float         fRol;
    float         fOldYaw;
    float         fOldPit;
    float         fOldRol;

};


#endif

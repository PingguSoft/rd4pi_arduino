#ifndef _CONFIG_H
#define _CONFIG_H

/*
*****************************************************************************************
* HAT BUTTONS
*****************************************************************************************
*/
#define HAT_NONE               0
#define HAT_UP                 1
#define HAT_RIGHT              2
#define HAT_DOWN               4
#define HAT_LEFT               8

//        1
//    9   |   3
//        |
//  8 ----0----  2
//        |
//   12   |   6
//        4


/*
*****************************************************************************************
* CONSTANTS
*****************************************************************************************
*/
#define NUM_LEDS                2
#define NUM_HAT_BUTTONS         1
#define NUM_LEFT_BUTTONS        5
#define NUM_RIGHT_BUTTONS       8
#define NUM_HW_BUTTONS          (NUM_LEFT_BUTTONS + NUM_RIGHT_BUTTONS)
#define NUM_HW_BUTTONS_HATS     (NUM_HW_BUTTONS + 4)                        // buttons + hat
#define NUM_KEYS                (NUM_HW_BUTTONS_HATS + (2 * 4))             // buttons + hat + 2 axis
    
// key bit
enum {
    // right
    BIT_A = 0,
    BIT_B,
    BIT_X,
    BIT_Y,
    BIT_R1,
    BIT_R2,
    BIT_RTHUMB,
    BIT_START,
    
    // left
    BIT_L1,
    BIT_L2,
    BIT_LTHUMB,
    BIT_SELECT,
    BIT_POWER,
    
    // hat
    BIT_HAT_UP,
    BIT_HAT_RIGHT,
    BIT_HAT_DOWN,
    BIT_HAT_LEFT,
    
    // analog axis
    BIT_AXIS_LX_MINUS,
    BIT_AXIS_LX_PLUS,
    BIT_AXIS_LY_MINUS,
    BIT_AXIS_LY_PLUS,    

    BIT_AXIS_RX_MINUS,
    BIT_AXIS_RX_PLUS,
    BIT_AXIS_RY_MINUS,
    BIT_AXIS_RY_PLUS
};

#define BIT_LEFT_HAT_BUTTONS    (NUM_RIGHT_BUTTONS + NUM_LEFT_BUTTONS)

#define I2C_SLAVE_ADDR_MICRO    0x20
#define I2C_SLAVE_ADDR_MINI     0x21

/*
*****************************************************************************************
* FEATURES
*****************************************************************************************
*/
#define __DEBUG__

/*
*****************************************************************************************
* LEFT ARDUINO PRO MINI PINS
*****************************************************************************************
*/

// buttons
#define PIN_BTN_HOME            7
#define PIN_BTN_LTHUMB          8
#define PIN_BTN_SELECT          10
#define PIN_BTN_L1              11
#define PIN_BTN_L2              12

// analog axis
#define PIN_A_AXIS_Y            A0
#define PIN_A_AXIS_X            A1
#define PIN_A_AXIS_HAT_Y        A2
#define PIN_A_AXIS_HAT_X        A3


// i2c for arduino communication
#define PIN_HW_SDA              A4
#define PIN_HW_SCL              A5

// i2c and irq for mpu-6050
#define PIN_SW_SDA              3
#define PIN_SW_SCL              4
#define PIN_IRQ                 2

// other signals
#define PIN_WS_2812_DATA        5
//#define PIN_PS_HOLD             6
#define PIN_LED                 13
//#define PIN_A_BATT              A6
#define PIN_A_DIAL              A7


/*
*****************************************************************************************
* COMMON DATA TYPES
*****************************************************************************************
*/
enum pad_mode {
    MODE_NONE = 0,
    MODE_JOYSTICK,
    MODE_KEYBOARD_MOUSE,
    MODE_CALIBRATION,
};

struct axis_range {
    u16     minX, maxX;
    u16     minY, maxY;
};

struct settings {
    u32                 dwMagicID;
    u8                  ucPadMode;
    struct axis_range   left;
    struct axis_range   right;
    u8                  keymap[NUM_KEYS];
};


#endif

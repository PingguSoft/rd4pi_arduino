#include <Arduino.h>
#include <Wire.h>
#include <Adafruit_NeoPixel.h>
#include "common.h"
#include "config.h"
#include "utils.h"
#include "I2CRpc.h"
#include "StatusLed.h"
#include "src/SoftI2CMaster/SoftWire.h"
#include "GyroMouse.h"

#undef Wire

/*
*****************************************************************************************
* CONSTANTS
*****************************************************************************************
*/
#define NUM_ANALOG_RETRY        5

// timeouts (ms)
#define TIMEOUT_SHIFT_MCLICKS   300
#define TIMEOUT_STICK_REPORT    10

#define MASK_BIT               (1 << (sizeof(TBL_PIN_BUTTONS) - 1))

// hatState buttons
#define HAT_ADC_NO_KEY         (1023 * 0.8)
#define HAT_ADC_RIGHT_UP       (1023 * 0.60)
#define HAT_ADC_RIGHT_DOWN     (1023 * 0.30)


/*
*****************************************************************************************
* TYPES
*****************************************************************************************
*/


/*
*****************************************************************************************
* CONSTS
*****************************************************************************************
*/
static const u8 PROGMEM TBL_PIN_BUTTONS[] = {
    PIN_BTN_L1, PIN_BTN_L2, PIN_BTN_LTHUMB, PIN_BTN_SELECT, PIN_BTN_HOME
};

static const u8 PROGMEM TBL_PIN_ANALOG[] = {
    PIN_A_AXIS_Y,       PIN_A_AXIS_X,
    PIN_A_AXIS_HAT_Y,   PIN_A_AXIS_HAT_X,
};


/*
*****************************************************************************************
* VARIABLES
*****************************************************************************************
*/
// for MPU-6050
SoftWire                    softI2C = SoftWire();

static StatusLed            mLed = StatusLed(PIN_WS_2812_DATA);
static GyroMouse            mGyro;
static u16                  mLastBtns;
static u16                  mLastX, mLastY;
static u8                   mLastMX, mLastMY, mLastMZ;
static long                 mLastPowerTS = 0;
static long                 mLastStickTS = 0;

// i2c slave
static u8                   mCmd;


/*
*****************************************************************************************
* FUNCTIONS
*****************************************************************************************
*/
u16 analogReadAvg(u8 pin) {
    u16 v;
    u32 sum = 0;
    u16 values[NUM_ANALOG_RETRY];

    u16 min = 1023;
    u16 max = 0;

    for (u8 i = 0; i < NUM_ANALOG_RETRY; i++) {
        v = analogRead(pin);
        values[i] = v;

        if (v < min) {
            min = v;
        }
        if (v > max) {
            max = v;
        }
        sum += v;
    }

    sum = sum - min - max;
    return sum / (NUM_ANALOG_RETRY - 2);
}


/*
*****************************************************************************************
* I2C slave events
*****************************************************************************************
*/
void receiveEvent(int len) {
//    u8 oldSREG = SREG;
    u8 cmd;
    u8 param;

//    cli();
    cmd = Wire.read();

    //LOG(F("cmd : %x len:%d\n"), cmd, len);
    switch (cmd) {
        case I2C_REG_SET_LED_RGB: {
            u8  index;
            u32 rgb = 0;
            u16 blink = 0;

            index = Wire.read();
            for (u8 i = 0; i < 3; i++) {
                rgb <<= 8;
                rgb |= Wire.read();
            }
            blink  = (Wire.read() << 8);
            blink |= Wire.read();
            mLed.set(index, rgb, blink);
        }
        break;

        case I2C_REG_SET_LED_BRIGHTNESS: {
            param = Wire.read();
            mLed.setBrightness(param);
        }
        break;
/*
        case I2C_REG_SET_POWER: {
            param = Wire.read();
            digitalWrite(PIN_PS_HOLD, param);
            if (param == 0) {
                for (;;);
            }
        }
        break;
*/
        default: {
            mCmd = cmd;
        }
        break;
    }
//    SREG = oldSREG;
}

void requestEvent() {
//    u8  oldSREG = SREG;
    u8  packet[9];
    u32 axis;

//    cli();
    u8  cmd = mCmd;
    mCmd = I2C_REG_NOP;
    
    switch (cmd) {
        case I2C_REG_GET_EVENTS: {
            packet[0] = 0xCA;
            Utils::put((u8*)&packet[1], mLastBtns);
            axis = ((u32)mLastY << 12) | (mLastX & 0x0fff);
            packet[3] = (axis >>  0) & 0xff;
            packet[4] = (axis >>  8) & 0xff;
            packet[5] = (axis >> 16) & 0xff;
            packet[6] = mLastMX;
            packet[7] = mLastMY;
            packet[8] = mLastMZ;
            Wire.write(packet, 9);
        }
        break;
    }
//    SREG = oldSREG;
}

/*
*****************************************************************************************
* SETUP
*****************************************************************************************
*/
void setup() {
//    pinMode(PIN_PS_HOLD, OUTPUT);
//    pinMode(PIN_A_BATT, INPUT);
//    digitalWrite(PIN_PS_HOLD, LOW);

    // keys and axis
    mLastBtns = 0;
    for (u8 i = 0; i < sizeof(TBL_PIN_BUTTONS); i++) {
        u8 pin = pgm_read_byte(&TBL_PIN_BUTTONS[i]);
        pinMode(pin, INPUT_PULLUP);
    }

    for (u8 i = 0; i < sizeof(TBL_PIN_ANALOG); i++) {
        u8 pin = pgm_read_byte(&TBL_PIN_ANALOG[i]);
        pinMode(pin, INPUT);
    }

    Serial.begin(115200);
    mGyro.setup();

    // i2c slave
    Wire.begin(I2C_SLAVE_ADDR_MINI);
    Wire.onReceive(receiveEvent);
    Wire.onRequest(requestEvent);
}

u8 getHatIndex(u8 hatState) {
    for (u8 i = 0; i < 4; i++) {
        if (hatState & (1 << i))
            return (i + 1);
    }
    return 0;
}


//
// process hat buttons
//
int getHat(void) {
    s16     adcHat;
    u8      hatState;

    int adcHatX = analogReadAvg(PIN_A_AXIS_HAT_X);
    if (adcHatX > HAT_ADC_NO_KEY) {
        hatState = HAT_NONE;
    } else if (adcHatX > HAT_ADC_RIGHT_UP) {
        hatState = HAT_RIGHT;
    } else {
        hatState = HAT_LEFT;
    }
    
    int adcHatY = analogReadAvg(PIN_A_AXIS_HAT_Y);
    if (adcHatY > HAT_ADC_NO_KEY) {
        hatState |= HAT_NONE;
    } else if (adcHatY > HAT_ADC_RIGHT_UP) {
        hatState |= HAT_UP;
    } else {
        hatState |= HAT_DOWN;
    }
    //LOG(F("hat_x:%4d hat_y:%4d state:%4d\n"), adcHatX, adcHatY, hatState);

    return hatState;

}

void process(long ts) {
    u8      pin;
    u8      state;
    u8      hat;
    u16     key_state;
    u16     x, y;

    if (ts - mLastStickTS > TIMEOUT_STICK_REPORT) {
        key_state = 0;

        for (u8 i = 0; i < sizeof(TBL_PIN_BUTTONS); i++) {
            pin   = pgm_read_byte(&TBL_PIN_BUTTONS[i]);
            state = digitalRead(pin);
            key_state >>= 1;
            key_state  |= (state ? 0 : MASK_BIT);
        }

        hat  = getHat();
        key_state |= (hat << NUM_LEFT_BUTTONS);
        if (key_state != mLastBtns) {
            mLastBtns = key_state;
//            LOG(F("%08x\n"), key_state);
        }

        //
        // analog sticks
        //
        x = 1023 - analogReadAvg(PIN_A_AXIS_X);
        y = 1023 - analogReadAvg(PIN_A_AXIS_Y);
        if (x != mLastX || y != mLastY) {
//            LOG(F("x:%4d, y:%4d\n"), x, y);
            mLastX = x;
            mLastY = y;
        }
        mLastStickTS = ts;
    }
}


/*
*****************************************************************************************
* LOOP
*****************************************************************************************
*/
long mLastBlinkTS = 0;

void loop() {
    long ts = millis();
    
    process(ts);
    mGyro.process(ts, &mLastMX, &mLastMY, &mLastMZ);
    mLed.loop(ts);
}


#ifndef _I2CRPC_H_
#define _I2CRPC_H_

#include "common.h"
#include "config.h"


/*
*****************************************************************************************
* CONSTANTS
*****************************************************************************************
*/
enum {
    I2C_REG_NOP = 0x00,
    I2C_REG_SET_LED_RGB,
    I2C_REG_SET_LED_BRIGHTNESS,
    I2C_REG_GET_EVENTS,
};


/*
*****************************************************************************************
* CLASS
*****************************************************************************************
*/


#endif

#include <Arduino.h>
#include <EEPROM.h>
#include "GyroMouse.h"
#include "src/MPU6050/MPU6050_6Axis_MotionApps20.h"

/*
*****************************************************************************************
* CONSTANTS
*****************************************************************************************
*/


/*
*****************************************************************************************
* MACROS
*****************************************************************************************
*/


/*
*****************************************************************************************
* TYPES
*****************************************************************************************
*/


/*
*****************************************************************************************
* CONSTS
*****************************************************************************************
*/


/*
*****************************************************************************************
* VARIABLES
*****************************************************************************************
*/
static GyroMouse *GyroMouse::instance;


/*
*****************************************************************************************
* 
*****************************************************************************************
*/
s8 GyroMouse::cut(int val) {
    s8 ret;

    ret = (val < -127) ? -127 : ((val > 127) ? 127 : val);
    return ret;
}

void GyroMouse::dmpDataReady(void) {
    instance->isMpuInterrupt = true;
}

float GyroMouse::delta(float val) {
    if (val > 180) {
        return val - 360;
    } else if (val < -180) {
        return val + 360;
    }
    return val;
}

/*
*****************************************************************************************
* SETUP
*****************************************************************************************
*/
void GyroMouse::setup(void) {
    GyroMouse::instance = this;

    isDmpReady = false;

    pinMode(PIN_LED, OUTPUT);
    pinMode(PIN_A_DIAL, INPUT);

    mpu.initialize();
    pinMode(PIN_IRQ, INPUT);
    if (!mpu.testConnection()) {
        LOG(F("MPU6050 connection failed\n"));
        return;
    }

    // load and configure the DMP
    LOG(F("Initializing DMP...\n"));
    devStatus = mpu.dmpInitialize();

    // supply your own gyro offsets here, scaled for min sensitivity
    mpu.setXGyroOffset(220);
    mpu.setYGyroOffset(76);
    mpu.setZGyroOffset(-85);
    mpu.setZAccelOffset(1788); // 1688 factory default for my test chip

    if (devStatus == 0) {
        // Calibration Time: generate offsets and calibrate our MPU6050
        mpu.CalibrateAccel(6);
        mpu.CalibrateGyro(6);
        mpu.PrintActiveOffsets();
        // turn on the DMP, now that it's ready
        LOG(F("Enabling DMP...\n"));
        mpu.setDMPEnabled(true);

        attachInterrupt(digitalPinToInterrupt(PIN_IRQ), &GyroMouse::dmpDataReady, RISING);
        mpuIntStatus = mpu.getIntStatus();

        LOG(F("DMP ready! Waiting for first interrupt...\n"));
        isDmpReady = true;

        // get expected DMP packet size for later comparison
        packetSize = mpu.dmpGetFIFOPacketSize();
    } else {
        // ERROR!
        // 1 = initial memory load failed
        // 2 = DMP configuration updates failed
        // (if it's going to break, usually the code will be 1)
        LOG(F("DMP Initialization failed (code %d)\n"), devStatus);
    }

    fOldYaw = 0;
    fOldPit = 0;
    fOldRol = 0;
}

/*
*****************************************************************************************
* LOOP
*****************************************************************************************
*/
u8 GyroMouse::process(long ts, s8 *mx, s8 *my, s8 *mz) {
    static long oldTS;
    u16         dial;
    u8          ret = 0;

    if (!isDmpReady) { // || !isMpuInterrupt) {
        return ret;
    }

    fifoCount = mpu.getFIFOCount();
    if (fifoCount < packetSize) {
        return ret;
    }

    isMpuInterrupt = false;
    mpuIntStatus   = mpu.getIntStatus();
    fifoCount      = mpu.getFIFOCount();
    
    if (fifoCount < packetSize) {
        
    } else if ((mpuIntStatus & _BV(MPU6050_INTERRUPT_FIFO_OFLOW_BIT)) || fifoCount >= 1024) {
        mpu.resetFIFO();
        LOG(F("FIFO overflow!\n"));
    } else if (mpuIntStatus & _BV(MPU6050_INTERRUPT_DMP_INT_BIT)) {
        while (fifoCount >= packetSize) {
            mpu.getFIFOBytes(fifoBuffer, packetSize);
            fifoCount -= packetSize;
        }

        // display Euler angles in degrees
        mpu.dmpGetQuaternion(&q, fifoBuffer);
        mpu.dmpGetGravity(&gravity, &q);
        mpu.dmpGetYawPitchRoll(ypr, &q, &gravity);

        fYaw = ypr[0] * 180 / M_PI;
        fPit = ypr[1] * 180 / M_PI;
        fRol = ypr[2] * 180 / M_PI;

        if (ts - oldTS > 20) {
            dial = 1023 - analogRead(PIN_A_DIAL);
            //LOG(F("dial:%d\n"), dial);
            
            float gain    = float(35 + dial) / 35.0f;
            int iYawDelta = (delta(fYaw - fOldYaw) * gain);
            int iPitDelta = -(delta(fPit - fOldPit) * gain);
            int iRolDelta = (delta(fRol - fOldRol) * gain);

            if (iYawDelta != 0 || iPitDelta != 0 || iRolDelta != 0) {
                *mx = cut(iYawDelta);   // x
                *my = cut(iPitDelta);   // y
                *mz = cut(iRolDelta);   // z

                //LOG(F("ypr : %5d, %5d, %5d (%5d, %5d, %5d) %4d %4d\n"), (int)fYaw, (int)fPit, (int)fRol, iYawDelta, iPitDelta, iRolDelta, dial, (int)(gain * 10));
                ret = 7;

                fOldYaw = fYaw;
                fOldPit = fPit;
                fOldRol = fRol;

                isBlink = !isBlink;
                digitalWrite(PIN_LED, isBlink);
                oldTS   = ts;
            }
        }
    }
    return ret;
}

#ifndef _STATUS_LED_H_
#define _STATUS_LED_H_
#include <Adafruit_NeoPixel.h>
#include "common.h"
#include "config.h"

/*
*****************************************************************************************
* CONSTANTS
*****************************************************************************************
*/
#define HUE(h)                  (65535L * (h) / 360)
#define SAT(s)                  (255L   * (s) / 100)
#define VAL(v)                  (255L   * (v) / 100)



#define COLOR_NONE              0x000000

// right led
#define COLOR_JOYSTICK          (u32)0x895200
#define COLOR_KEYBOARD_MOUSE    (u32)0x23347F
#define COLOR_CALIBRATION       (u32)0x107F00
#define COLOR_BATTERY_WARN      (u32)0x7F0000
#define COLOR_POWER_OFF         (u32)0x7F0000

#define COLOR_USB               (u32)0x00FCFF4F
#define COLOR_USB_HSV           (u32)0x003D4564

#define COLOR_BLUETOOTH         (u32)0x00001DFF
#define COLOR_BLUETOOTH_HSV     (u32)0x00E96464

#define COLOR_I2C               (u32)0x00FF00D8
#define COLOR_I2C_HSV           (u32)0x01356464



/*
*****************************************************************************************
* CLASS
*****************************************************************************************
*/
class StatusLed {
public:
    StatusLed(u8 pin);
    void set(u8 index, u32 rgb, u16 blink);
    void setBrightness(u8 brightness);
    void loop(long ts);

private:
    bool    mIsLedUpdated;
    u16     mLedBlinkPeriod[NUM_LEDS];
    long    mLastBlinkTS[NUM_LEDS];
    u32     mLedRGB[NUM_LEDS];
    
    Adafruit_NeoPixel mStrip;
};


#endif


#include <Arduino.h>
#include "StatusLed.h"
#include "utils.h"

/*
*****************************************************************************************
* VARIABLES
*****************************************************************************************
*/

/*
*****************************************************************************************
* Functions
*****************************************************************************************
*/
StatusLed::StatusLed(u8 pin) {
    mIsLedUpdated   = false;

    for (u8 i = 0; i < NUM_LEDS; i++) {
        mLedRGB[i]         = 0;    
        mLastBlinkTS[i]    = 0;
        mLedBlinkPeriod[i] = 0;
    }
    mStrip = Adafruit_NeoPixel(NUM_LEDS, pin, NEO_GRB + NEO_KHZ800);    
    mStrip.begin();
    mStrip.clear();
}

void StatusLed::set(u8 index, u32 rgb, u16 blink) {
    mStrip.setPixelColor(index, rgb);
    mLedRGB[index] = mStrip.getPixelColor(index);
    if (blink > 0) {
        mLastBlinkTS[index] = 0;
    }
    mLedBlinkPeriod[index] = blink;
    mIsLedUpdated   = true;
}

/*
void StatusLed::setRGB(u8 index, u8 r, u8 g, u8 b, u16 blink) {
    u32 rgb = Adafruit_NeoPixel::Color(r, g, b);
    set(index, rgb, blink);
}

void StatusLed::setHSV(u8 h, u8 s, u8 v, u16 blink) {
    u32 rgb = Adafruit_NeoPixel::gamma32(Adafruit_NeoPixel::ColorHSV(HUE(h), SAT(s), VAL(v)));
    set(rgb, blink);
}
*/

void StatusLed::setBrightness(u8 brightness) {
    mStrip.setBrightness(brightness);
    for (u8 i = 0; i < NUM_LEDS; i++) {
        mLedRGB[i] = mStrip.getPixelColor(i);
    }
    mIsLedUpdated   = true;
}

void StatusLed::loop(long ts) {
    for (u8 i = 0; i < NUM_LEDS; i++) {
        if (mLedBlinkPeriod[i] > 0) {
            if (ts - mLastBlinkTS[i] > mLedBlinkPeriod[i]) {
                u32 color = (mStrip.getPixelColor(i) == 0) ? mLedRGB[i] : 0;
                mStrip.setPixelColor(i, color);
                mLastBlinkTS[i] = ts;
                mIsLedUpdated   = true;
            }
        }
    }

    if (mIsLedUpdated) {
        mStrip.show();
        mIsLedUpdated = false;
    }
}



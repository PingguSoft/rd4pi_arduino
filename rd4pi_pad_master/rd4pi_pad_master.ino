#include <Arduino.h>
#include <EEPROM.h>
#include <Wire.h>
#include <USBAPI.h>
#include <Joystick.h>
#include <Keyboard.h>
#include "Mouse.h"
#include "common.h"
#include "config.h"
#include "utils.h"
#include "I2CRpc.h"
#include "StatusLed.h"
#include "RNBTHid.h"
#include "EmulMCP23017.h"

/*
*****************************************************************************************
* CONSTANTS
*****************************************************************************************
*/
#define MAGIC_ID                0xcafebeb1

// timeouts (ms)
#define TIMEOUT_SHIFT_MCLICKS   300
#define TIMEOUT_STICK_REPORT    20

#define NUM_ANALOG_RETRY        5
#define BIT_KEY_MASK            BV(NUM_RIGHT_BUTTONS - 1)

#define MOUSE_LBTN              0xFD
#define MOUSE_RBTN              0xFE
#define MOUSE_WHEEL             0xFF

/*
*****************************************************************************************
* TYPES
*****************************************************************************************
*/


/*
*****************************************************************************************
* CONSTS TABLES
*****************************************************************************************
*/
static const u8 PROGMEM TBL_PIN_BUTTONS[] = {
    PIN_BTN_A,  PIN_BTN_B,  PIN_BTN_X,      PIN_BTN_Y,
    PIN_BTN_R1, PIN_BTN_R2, PIN_BTN_RTHUMB, PIN_BTN_START,
};

static const u8 PROGMEM TBL_PIN_ANALOG[] = {
    PIN_A_AXIS_Y,  PIN_A_AXIS_X,
};

static const s16 PROGMEM TBL_ANGLES[] = {
     -1,   0,  90,  45,
    180,  -1, 135,  -1,
    270, 315,  -1,  -1,
    225
};

static const u8 PROGMEM TBL_QUAKE_KEYMAP[] = {
    MOUSE_LBTN, ' ',  'c',  KEY_TAB, '/', 0, MOUSE_WHEEL, KEY_RIGHT_ALT,
    0, 0, KEY_END, KEY_ESC, 0, KEY_PAGE_DOWN, 0, KEY_DELETE, 
    0, 'a', 'd', 'w', 's', KEY_LEFT_ARROW, KEY_RIGHT_ARROW, KEY_UP_ARROW, 
    KEY_DOWN_ARROW
};

/*
*****************************************************************************************
* VARIABLES
*****************************************************************************************
*/
static I2CRpcClient         mRemote;

static struct settings      mSetting;
static bool                 mIsCalMode     = false;
static enum pad_mode        mPrevPadMode;
static long                 mLastStickTS   = 0;
static long                 mLastPowerTS   = 0;
static long                 mPowerOffTS    = 0;


static u32                  mLastBtns;
static Joystick_            *m_pJoystick = NULL;
static RNBTHid              mBtHid = RNBTHid();
static EmulMCP23017         mMcp   = EmulMCP23017();

/*
*****************************************************************************************
* FUNCTIONS
*****************************************************************************************
*/
u16 analogReadAvg(u8 pin) {
    u16 v;
    u32 sum = 0;
    u16 values[NUM_ANALOG_RETRY];

    u16 min = 1023;
    u16 max = 0;

    for (u8 i = 0; i < NUM_ANALOG_RETRY; i++) {
        v = analogRead(pin);
        values[i] = v;

        if (v < min) {
            min = v;
        }
        if (v > max) {
            max = v;
        }
        sum += v;
    }

    sum = sum - min - max;
    return sum / (NUM_ANALOG_RETRY - 2);
}

void setAxisRange(struct settings *info) {
    if (m_pJoystick != NULL && info != NULL) {
        m_pJoystick->setXAxisRange(info->left.minX, info->left.maxX);
        m_pJoystick->setYAxisRange(info->left.minY, info->left.maxY);
        m_pJoystick->setZAxisRange(info->right.minX, info->right.maxX);
        m_pJoystick->setRzAxisRange(info->right.minY, info->right.maxY);
    }
}

/*
*****************************************************************************************
* SETUP
*****************************************************************************************
*/
void setup() {
    pinMode(PIN_PS_HOLD, INPUT);

    Serial.begin(115200);   // usb
  
    mLastBtns = 0;
    for (u8 i = 0; i < sizeof(TBL_PIN_BUTTONS); i++) {
        u8 pin = pgm_read_byte(&TBL_PIN_BUTTONS[i]);
        pinMode(pin, INPUT_PULLUP);
    }

    for (u8 i = 0; i < sizeof(TBL_PIN_ANALOG); i++) {
        u8 pin = pgm_read_byte(&TBL_PIN_ANALOG[i]);
        pinMode(pin, INPUT);
    }

    EEPROM.get(0, mSetting);
    LOG(F("magic:%lx\n"), mSetting.dwMagicID);
    if (mSetting.dwMagicID != MAGIC_ID) {
        mSetting.dwMagicID = MAGIC_ID;
        mSetting.left.minX = 230;
        mSetting.left.maxX = 840;
        mSetting.left.minY = 230;
        mSetting.left.maxY = 840;
        mSetting.right= mSetting.left;
        mSetting.ucPadMode = MODE_JOYSTICK;
        for (u8 i = 0; i < sizeof(mSetting.keymap); i++) {
            mSetting.keymap[i] = pgm_read_byte(&TBL_QUAKE_KEYMAP[i]);
        }
    }
    for (u8 i = 0; i < sizeof(mSetting.keymap); i++) {
        mSetting.keymap[i] = pgm_read_byte(&TBL_QUAKE_KEYMAP[i]);
    }    

    LOG(F("usb configured:%d\n"), USBDevice.configured());

    m_pJoystick = new Joystick_(JOYSTICK_DEFAULT_REPORT_ID,JOYSTICK_TYPE_GAMEPAD,
                  NUM_HW_BUTTONS, NUM_HAT_BUTTONS,    // Button Count, Hat Switch Count
                  true,  true,  true,           // X and Y, Z Axis
                  false, false, true,           // Rx, Ry, or Rz
                  false, false,                 // rudder or throttle
                  false, false, false);         // accelerator, brake, or steering

    m_pJoystick->begin();
    setAxisRange(&mSetting);
    Mouse.begin();

    mRemote.setLedBrightness(LED_RIGHT, 20);
    if (mSetting.ucPadMode != MODE_JOYSTICK && mSetting.ucPadMode != MODE_KEYBOARD_MOUSE) {
        mSetting.ucPadMode = MODE_JOYSTICK;
    }
}


/*
*****************************************************************************************
* BUTTON HANDLING
*****************************************************************************************
*/
bool isOnlyPressed(u16 changed, u16 state, u16 btn) {
    if ((changed & btn) == btn &&  (state & btn) == btn) {
        return true;
    }
    return false;
}

bool isPressed(u16 changed, u16 state, u16 btn) {
    if ((changed & btn) &&  (state & btn)) {
        return true;
    }
    return false;
}

bool isReleased(u16 changed, u16 state, u16 btn) {
    if ((changed & btn) &&  !(state & btn)) {
        return true;
    }
    return false;
}


/*
*****************************************************************************************
* EVENT HANDLING
*****************************************************************************************
*/
static const u32 tblColors[] = {
    COLOR_NONE, 
    COLOR_JOYSTICK, 
    COLOR_KEYBOARD_MOUSE, 
    COLOR_CALIBRATION
};

void setPadMode(enum pad_mode newMode) {
    bool isSkip = false;
    
    Keyboard.releaseAll();
    Mouse.release(0xff);
    mBtHid.mKeyboard.releaseAll();
    mBtHid.mMouse.releaseAll();

    if (mIsCalMode) {
        setAxisRange(&mSetting);
        mIsCalMode = false;
        if (newMode == MODE_CALIBRATION) {
            newMode = mSetting.ucPadMode;
        }
        if (mSetting.ucPadMode == newMode) {
            isSkip = true;
        }
        EEPROM.put(0, mSetting);
        LOG(F("cal mode exit\n"));
    } else if (newMode == MODE_CALIBRATION) {
        // set invalid range for calibration
        mSetting.left.minX = 8192;
        mSetting.left.maxX = 0;
        mSetting.left.minY = 8192;
        mSetting.left.maxY = 0;
        mSetting.right= mSetting.left;
        mIsCalMode = true;
        isSkip = true;
        LOG(F("cal mode enter\n"));
    }

    LOG(F("pad mode : %d , skip:%d\n"), newMode, isSkip);
    mRemote.setLed(LED_RIGHT, tblColors[newMode], (newMode == MODE_CALIBRATION) ? 200 : 0);

    mSetting.ucPadMode = newMode;
    if (!isSkip) {
        mBtHid.setMode(newMode);
    }
}

u8 getKey(u8 idx) {
    return mSetting.keymap[idx];
}

void processAxis(u16 val, u16 mid, u8 idxLittle, u8 idxBig, u8 *mask) {
    u32 maskLit = (u32)BV(idxLittle - BIT_AXIS_LX_MINUS);
    u32 maskBig = (u32)BV(idxBig    - BIT_AXIS_LX_MINUS);

    u8  keyBig = getKey(idxBig);
    u8  keyLit = getKey(idxLittle);
    u16 thr = mid * 3 / 10;
    
    if (val < mid - thr) {
        if (*mask & maskBig) {
            Keyboard.release(keyBig);
            mBtHid.mKeyboard.release(keyBig);
            *mask = *mask & ~maskBig;
        }
        Keyboard.press(keyLit);
        mBtHid.mKeyboard.press(keyLit);
        *mask = *mask | maskLit;
    } else if (val > mid + thr) {
        if (*mask & maskLit) {
            Keyboard.release(keyLit);
            mBtHid.mKeyboard.release(keyLit);
            *mask = *mask & ~maskLit;
        }
        Keyboard.press(keyBig);
        mBtHid.mKeyboard.press(keyBig);
        *mask = *mask | maskBig;
    } else {
        if (*mask & maskLit) {
            Keyboard.release(keyLit);
            mBtHid.mKeyboard.release(keyLit);
            *mask = *mask & ~maskLit;
        }
        if (*mask & maskBig) {
            Keyboard.release(keyBig);
            mBtHid.mKeyboard.release(keyBig);
            *mask = *mask & ~maskBig;
        }
    }
    //LOG(F("%d-%d, val:%4d, mid:%4d, thr:%4d, mask:%08x\n"), idxLittle, idxBig, val, mid, thr, *mask);
}

void processHid(u32 btns, u16 lx, u16 ly, u16 rx, u16 ry, s8 mx, s8 my) {
    static u32 lastBtns;
    static u16 lastLX, lastLY;
    static u16 lastRX, lastRY;
    static s8  lastMX, lastMY;
    static u8  lastMask;

    const u32 kBtnChgMask = btns ^ lastBtns;

    //
    // analog axis
    //

    //LOG(F("%08ld : %4d [%4d-%4d], %4d [%4d-%4d]\n"), millis(), lx, mSetting.left.minX, mSetting.left.maxX, 
    //                                                           ly, mSetting.left.minY, mSetting.left.maxY);

    // left analog with USB connected should be reported regularly
    if (mSetting.ucPadMode == MODE_JOYSTICK) {
        lx = constrain(lx, mSetting.left.minX, mSetting.left.maxX);
        m_pJoystick->setXAxis(lx);
        ly = constrain(ly, mSetting.left.minY, mSetting.left.maxY);            
        m_pJoystick->setYAxis(ly);

        if (lastLX != lx || lastLY != ly) {
            mBtHid.mJoystick.setXAxis(map(lx, mSetting.left.minX, mSetting.left.maxX, -127, 127));
            mBtHid.mJoystick.setYAxis(map(ly, mSetting.left.minY, mSetting.left.maxY, -127, 127));
            lastLX = lx;
            lastLY = ly;
        }
    } else if (mSetting.ucPadMode == MODE_KEYBOARD_MOUSE) {
        if (lastLX != lx || lastLY != ly) {
            u16 mid = (mSetting.left.minX + mSetting.left.maxX) >> 1;
            processAxis(lx, mid, BIT_AXIS_LX_MINUS, BIT_AXIS_LX_PLUS, &lastMask);

            mid = (mSetting.left.minY + mSetting.left.maxY) >> 1;
            processAxis(ly, mid, BIT_AXIS_LY_MINUS, BIT_AXIS_LY_PLUS, &lastMask);            

            lastLX = lx;
            lastLY = ly;
        }
    }

    if (lastRX != rx || lastRY != ry) {
        if (mSetting.ucPadMode == MODE_JOYSTICK) {
            rx = constrain(rx, mSetting.right.minX, mSetting.right.maxX);
            m_pJoystick->setZAxis(rx);
            ry = constrain(ry, mSetting.right.minY, mSetting.right.maxY);
            m_pJoystick->setRzAxis(ry);
            mBtHid.mJoystick.setZAxis(map(rx, mSetting.right.minX, mSetting.right.maxX, -127, 127));
            mBtHid.mJoystick.setRzAxis(map(ry, mSetting.right.minY, mSetting.right.maxY, -127, 127));
        } else if (mSetting.ucPadMode == MODE_KEYBOARD_MOUSE) {
            u16 mid = (mSetting.right.minX + mSetting.right.maxX) >> 1;
            processAxis(rx, mid, BIT_AXIS_RX_MINUS, BIT_AXIS_RX_PLUS, &lastMask);

            mid = (mSetting.right.minY + mSetting.right.maxY) >> 1;
            processAxis(ry, mid, BIT_AXIS_RY_MINUS, BIT_AXIS_RY_PLUS, &lastMask);
        }
        lastRX = rx;
        lastRY = ry;
    }

    //
    // hat
    //
    u8  newHat  = (btns     >> BIT_LEFT_HAT_BUTTONS) & 0x0f;
    u8  oldHat  = (lastBtns >> BIT_LEFT_HAT_BUTTONS) & 0x0f;
    if (newHat != oldHat) {
        if (mSetting.ucPadMode == MODE_JOYSTICK) {
            m_pJoystick->setHatSwitch(0, pgm_read_word(&TBL_ANGLES[newHat]));
        }
    }


    u32 changed = kBtnChgMask;        
    lastBtns = btns;

    //
    // mouse
    //
    if (mSetting.ucPadMode == MODE_KEYBOARD_MOUSE) {
        if (mx != lastMX || my != lastMY || changed) {
            btns = lastBtns;
            
            for (u8 i = 0; i < NUM_HW_BUTTONS_HATS; i++) {
                u8 key = getKey(i);
                
                if ((MOUSE_LBTN <= key && key <= MOUSE_WHEEL) && (changed & 0x01)) {
                    if (btns & 0x01) {
                        Mouse.press(BV(key - MOUSE_LBTN));
                        mBtHid.mMouse.press(BV(key - MOUSE_LBTN));
                    } else {
                        Mouse.release(BV(key - MOUSE_LBTN));
                        mBtHid.mMouse.release(BV(key - MOUSE_LBTN));
                    }
                    //LOG(F("mouse btn:%d %x\n"), i, (key - MOUSE_LBTN));
                }
                changed >>= 1;
                btns    >>= 1;
            }
            mBtHid.mMouse.move(mx, my, 0);
            Mouse.move(mx, my, 0);

            lastMX = mx;
            lastMY = my;
        }
    }

    //
    // buttons
    //
    changed = kBtnChgMask;
    btns    = lastBtns;
    if (mSetting.ucPadMode == MODE_JOYSTICK) {
        for (u8 i = 0; i < NUM_HW_BUTTONS; i++) {
            if (changed & 0x01) {
                m_pJoystick->setButton(i, btns & 0x01);
                mBtHid.mJoystick.setButton(i, btns & 0x01);
            }
            changed >>= 1;
            btns    >>= 1;
        }
    } else if (mSetting.ucPadMode == MODE_KEYBOARD_MOUSE) {
        for (u8 i = 0; i < NUM_HW_BUTTONS_HATS; i++) {
            u8 key = getKey(i);
            
            if ((0 < key && key < MOUSE_LBTN) && (changed & 0x01)) {
                //aLOG(F("key btn :%d %x\n"), i, key);
                if (btns & 0x01) {
                    Keyboard.press(key);
                    mBtHid.mKeyboard.press(key);
                } else {
                    Keyboard.release(key);
                    mBtHid.mKeyboard.release(key);
                }
            }
            changed >>= 1;
            btns    >>= 1;
        }
    }
    mBtHid.update();
}

void processCalibration(u16 lx, u16 ly, u16 rx, u16 ry) {
    mSetting.left.minX = MIN(lx, mSetting.left.minX);
    mSetting.left.maxX = MAX(lx, mSetting.left.maxX);
    mSetting.left.minY = MIN(ly, mSetting.left.minY);
    mSetting.left.maxY = MAX(ly, mSetting.left.maxY);
    mSetting.right.minX = MIN(rx, mSetting.right.minX);
    mSetting.right.maxX = MAX(rx, mSetting.right.maxX);
    mSetting.right.minY = MIN(ry, mSetting.right.minY);
    mSetting.right.maxY = MAX(ry, mSetting.right.maxY);        
    LOG(F("LX:%4d - %4d, LY:%4d - %4d, RX:%4d - %4d, RY:%4d - %4d\n"),
        mSetting.left.minX, mSetting.left.maxX, mSetting.left.minY, mSetting.left.maxY,
        mSetting.right.minX, mSetting.right.maxX, mSetting.right.minY, mSetting.right.maxY);
}

void process(long ts) {
    u8      pin;
    u8      state;
    u16     rightX, rightY;
    u32     newBtns;
    u8      newHat;
    u8      oldHat;
static bool isFirst = true;   

    if (IS_ELAPSED(mLastStickTS, ts, TIMEOUT_STICK_REPORT)) {
        u16 leftBtn, leftX, leftY;
        s8  mx, my, mz;

        newBtns = 0;
        newHat  = 0;
        oldHat  = 0;

        if (!mRemote.getEvents(&leftBtn, &leftX, &leftY, &mx, &my, &mz)) {
            leftBtn = 0;
            leftX   = (mSetting.left.minX + mSetting.left.maxX) / 2;
            leftY   = (mSetting.left.minY + mSetting.left.maxY) / 2;
            mx      = 0;
            my      = 0;
            mz      = 0;
            LOG(F("rpc fail\n"));
        } else if (isFirst) {
            for (u8 i = 0; i < NUM_LEDS; i++) {
                mRemote.setLed(i, mRemote.getLed(i), mRemote.getLedBlink(i));
            }
            isFirst = false;
        }
        
        for (u8 i = 0; i < sizeof(TBL_PIN_BUTTONS); i++) {
            pin   = pgm_read_byte(&TBL_PIN_BUTTONS[i]);
            state = digitalRead(pin);
            newBtns >>= 1;
            newBtns  |= (state ? 0 : BIT_KEY_MASK);
        }
        newBtns |= ((u32)leftBtn << NUM_RIGHT_BUTTONS);
       
        if (newBtns != mLastBtns) {
            u32 changed = newBtns ^ mLastBtns;

            //
            // check power button only press for power off
            //
            if (isOnlyPressed(changed, newBtns, BV(BIT_START))) {
                mLastPowerTS = ts;
            } else {
                mLastPowerTS = 0;
            }

            oldHat  = (mLastBtns >> BIT_LEFT_HAT_BUTTONS) & 0x0f;
            newHat  = (newBtns   >> BIT_LEFT_HAT_BUTTONS) & 0x0f;

            mLastBtns = newBtns;
            //LOG(F("button : %08lx\n"), newBtns);
        }

        //
        // analog sticks
        //
        rightX = analogReadAvg(PIN_A_AXIS_X);
        rightY = analogReadAvg(PIN_A_AXIS_Y);

        if (mIsCalMode) {
            processCalibration(leftX, leftY, rightX, rightY);
        }
        processHid(newBtns, leftX, leftY, rightX, rightY, mx, my);
        mMcp.process(&mSetting, newBtns, leftX, leftY, rightX, rightY, mx, my);

        //
        // turn off
        //
        if (mPowerOffTS == 0 && IS_ELAPSED_NZ(mLastPowerTS, ts, 5000)) {
            LOG(F("turn off start\n"));
            mRemote.setLed(LED_LEFT, 0, 0);
            mRemote.setLed(LED_RIGHT, COLOR_POWER_OFF, 100);
            mPowerOffTS = ts;
        }

        //
        // special power button combination
        //
        if ((newBtns & BV(BIT_HOME)) && (newHat != oldHat)) {
            LOG(F("hat:%d\n"), newHat);
            switch (newHat) {
                case HAT_LEFT:
                    setPadMode(MODE_JOYSTICK);
                    break;

                case HAT_RIGHT:
                    setPadMode(MODE_KEYBOARD_MOUSE);
                    break;

                case HAT_UP:
                    setPadMode(MODE_CALIBRATION);
                    break;

                case HAT_DOWN:
                    bool en = !mBtHid.isSleep();
                    LOG(F("bluetooth sleep : %d\n"), en);
                    mBtHid.sleep(en, true);
                    if (!en) {
                        setPadMode(mSetting.ucPadMode);
                    }
                    break;
            }
        }
        mLastStickTS = ts;
    }
}


/*
*****************************************************************************************
* LOOP
*****************************************************************************************
*/
static bool     mIsInc = true;
static u8       mVal = 0;
static long     mLastLed = 0;
static long     mLastPwrCheck = 0;
static bool     mIsPwrOnCheck = true;

void loop() {
    long ts = millis();

    if (mIsPwrOnCheck) {
        if (digitalRead(PIN_PS_HOLD) == LOW) {
            // powerd by START key
            if (mLastPwrCheck == 0) {
                mLastPwrCheck = ts;
            } else if (IS_ELAPSED(mLastPwrCheck, ts, 3000)) {
                pinMode(PIN_PS_HOLD, OUTPUT);
                digitalWrite(PIN_PS_HOLD, HIGH);
                mIsPwrOnCheck = false;
            }
        } else {
            mIsPwrOnCheck = false;
        }

        if (!mIsPwrOnCheck) {
             mBtHid.setup();
            setPadMode(mSetting.ucPadMode);
        }
    } else {
        process(ts);

        if (IS_ELAPSED(mLastLed, ts, 20)) {
            u32 rgb = mBtHid.isSleep() ? (USBDevice.configured() ? COLOR_USB : COLOR_I2C) : COLOR_BLUETOOTH;

            mRemote.setLedBrightness(LED_LEFT, mVal);
            mRemote.setLed(LED_LEFT, rgb, 0);
            mVal = (mIsInc) ? (mVal + 1) : (mVal - 1);
            if (mVal == 0 || mVal == 30) {
                mIsInc = !mIsInc;
            }
            mLastLed = ts;
        }

        mBtHid.loop(ts);

        if (IS_ELAPSED_NZ(mPowerOffTS, ts, 1000)) {
            mRemote.setLed(LED_LEFT, 0, 0);
            mRemote.setLed(LED_RIGHT, 0, 0);

            LOG(F("OFF !!\n"));
            for(;;);
        }
    }
}


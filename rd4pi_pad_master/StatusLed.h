#ifndef _STATUS_LED_H_
#define _STATUS_LED_H_
#include <Adafruit_NeoPixel.h>
#include "common.h"

/*
*****************************************************************************************
* CONSTANTS
*****************************************************************************************
*/
#define HUE(h)                  (65535L * (h) / 360)
#define SAT(s)                  (255L   * (s) / 100)
#define VAL(v)                  (255L   * (v) / 100)


#define LED_LEFT                0
#define LED_RIGHT               1

#define COLOR_NONE              0x000000

// right led
#define COLOR_JOYSTICK          (u32)0x895200
#define COLOR_KEYBOARD_MOUSE    (u32)0x00FFFF
#define COLOR_CALIBRATION       (u32)0x107F00
#define COLOR_BATTERY_WARN      (u32)0x7F0000
#define COLOR_POWER_OFF         (u32)0xFFFFFF

#define COLOR_USB               (u32)0x00FCFF4F
#define COLOR_BLUETOOTH         (u32)0x00001DFF
#define COLOR_I2C               (u32)0x00FF00D8



/*
*****************************************************************************************
* CLASS
*****************************************************************************************
*/
class StatusLed {
public:
    StatusLed(u8 pin);
    void set(u32 rgb, u16 blink);
    void setRGB(u8 r, u8 g, u8 b, u16 blink);
    void setHSV(u8 h, u8 s, u8 v, u16 blink);
    void setBrightness(u8 brightness);
    void loop(long ts);

private:
    u16     mLedBlinkPeriod;
    bool    mIsLedUpdated;
    long    mLastBlinkTS;
    u32     mLedRGB;
    
    Adafruit_NeoPixel mStrip;
};


#endif

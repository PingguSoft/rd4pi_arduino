#include <Arduino.h>
#include <Keyboard.h>
#include "utils.h"
#include "RNBTHid.h"


/*
*****************************************************************************************
* CONST
*****************************************************************************************
*/
#define NUM_RETRIAL     20

/*
*****************************************************************************************
* VARIABLES
*****************************************************************************************
*/


/*
*****************************************************************************************
* CONST TABLE
*****************************************************************************************
*/
static const PROGMEM char TBL_COMMANDS[] = {
    "SM,4\n"            // DTR mode
    "S~,6\n"            // HID profile
    "SQ,16\n"           // low latency
    "ST,255\n"          // Continous configuration, local and remote.
    "SW,0000\n"         // deep sleep enabled, sniff:0 * 0.625us = disabled
//  "SH,0233\n"         // 20x:keyboard, 21x:gamepad, 22x:mouse, 23x:combo(keyboard+mouse), 24x:joystick
//  "SR,9476B7719F52\n" // Remote Address (Use  SR,Z  to Remove). PC:34E12DC58112
};

static const PROGMEM char TBL_SLEEP[] = {
    "Q\r\n"             // Quiet, Turn off Discovery and Connectability.
    "Z\r\n"
};

static const PROGMEM char TBL_REBOOT[] = {
    "R,1\r\n"
};


/*
*****************************************************************************************
* Functions
*****************************************************************************************
*/
RNBTHid::RNBTHid() {
    m_isConnected   = false;
    m_isSleep       = false;
    m_isCommandMode = false;
}

void RNBTHid::setup(void) {
    pinMode(PIN_BT_CONNECT, OUTPUT);
    digitalWrite(PIN_BT_CONNECT, LOW);

    Serial1.begin(115200);
    sleep(true, true);
}

void RNBTHid::enterCommandMode(void) {
    if (m_isCommandMode)
        return;

    digitalWrite(PIN_BT_CONNECT, LOW);
    long start = millis();
    while (!m_isCommandMode && !IS_ELAPSED(start, millis(), 1000)) {
        LOG(F("$"));
        Serial1.write('$');
        Serial1.flush();
        delay(20);
        if (waitResp(F("CMD\r\n"), 0)) {
            m_isCommandMode = true;
            return;
        }
    }
    Serial1.print(F("\r\n"));
    Serial1.flush();
}

u8 RNBTHid::waitRx(u8 cnt, u16 wait, bool isEmpty) {
    u8      rx;
    u8      rd;
    long    start = millis();
    
    if (isEmpty) {
        while (cnt > 0) {
            if (Serial1.available()) {
                rd = Serial1.read();
                LOG(F("%c"), rd);
                cnt--;
            } else {
                delay(10);
            }
            
            if (IS_ELAPSED(start, millis(), wait)) {
                LOG(F("TOUT1!\n"));
                return 0;
            }
        }
    } else {
        while (!IS_ELAPSED(start, millis(), wait)) {
            rx = Serial1.available();
            if (rx >= cnt) {
                return rx;
            } else {
                delay(10);
            }
        }
        LOG(F("TOUT2!\n"));
        if (rx < cnt) {
            rx = 0;
        }
    }
    return rx;
}

bool RNBTHid::waitResp(const char *resp, u16 wait) {
    bool ret = false;
    u8   rx;
    int  rd;
    u8   cnt;
    bool isRun = true;
    long start = millis();

    while (isRun && !IS_ELAPSED(start, millis(), MAX(wait, 100))) {
        rd = Serial1.peek();
        if (rd >= 0) {
            LOG(F("B:%c [%x]\n"), rd, rd);

            switch (rd) {
                case '%':
                    rd = Serial1.read();    // remove %
                    
                    rx = waitRx(1, 100);
                    rd = Serial1.read();
                    LOG(F("CDN:%c\n"), rd);
                    
                    if (rd == 'C') {        // %CONNECT,000000000000,2
                        cnt = 21;
                        m_isConnected   = true;
                        m_isCommandMode = false;
                    } else if (rd == 'D') { // %DISCONNECT
                        cnt = 9;
                        m_isConnected = false;
                    } else if (rd == 'N') { // %NEW_PAIRING
                        cnt = 10;
                    } else if (rd == 'R') { // %REBOOT
                        cnt = 5;
                        m_isCommandMode = false;
                    }
                    break;

                case '!':
                    cnt = 3;
                    m_isCommandMode = true;
                    break;
                    
                case '?':
                    cnt = 3;
                    m_isCommandMode = true;
                    break;
                    
                case 'E':                   // ERR
                    cnt = 5;
                    m_isCommandMode = true;
                    break;

                case '\r':
                case '\n':
                    cnt = 1;
                    break;

                default:
                    if (m_isSleep) {
                        cnt = Serial1.available();
                    } else {
                        cnt = 0;
                    }
                    break;
            }
            
            if (cnt > 0) {
                //cnt = Serial1.available();
                //LOG(F("%c => wait:%d\n"), rd, cnt);
                rx = waitRx(cnt, MAX(wait, 100), true);

                // retry with command
                if (resp != NULL) {
                    return ret;
                }
            } else {
                isRun = false;
            }
        }else { 
            if (wait == 0) {
                isRun = false;
                break;
            } else {
                delay(10);
            }
        }
    }

    if (IS_ELAPSED(start, millis(), MAX(wait, 100))) {
        return ret;
    }

    if (resp != NULL) {
        if (wait == 0 && rd < 0) {
            return ret;
        }
        
        u8 max = strlen_P(resp);
        u8 ucChkIdx = 0;
        
        rx = waitRx(max, wait);
        for (u8 j = 0; j < rx; j++) {
            u8 ch = pgm_read_byte(resp + ucChkIdx);
            rd = Serial1.read();

            LOG(F("[%d] %c/%c, "), ucChkIdx, rd, ch);

            if (rd == ch) {
                ucChkIdx++;
                if (ucChkIdx == max) {
                    ret = true;
                    break;
                }
            } else {
                ucChkIdx = 0;
            }
        }
    }

    return ret;
}

bool RNBTHid::waitResp(const __FlashStringHelper *resp, u16 wait) {
    return waitResp((const char *)resp, wait);
}

void RNBTHid::sendCommands(const char *tbl, u16 wait) {
    u8 idx = 0;
    u8 cmd;
    u8 i;
    const char *ack;

    for (u8 j = 0; j < NUM_RETRIAL; j++) {
        LOG(F("COMMANDS:"));
        for (i = 0; i < strlen_P(tbl); i++) {
            char ch = pgm_read_byte(tbl + i);
            if (idx++ == 0) {
                cmd = ch;
            }
            Serial1.write(ch);
            LOG(F("%c"), ch);
            if (ch == '\n') {
                delay(20);
                idx = 0;

                switch (cmd) {
                    case 'Q':
                        ack = PSTR("Quiet\r\n");
                        break;

                    case 'R':
                        ack = PSTR("Reboot!\r\n");
                        break;

                    case 'Z':
                        ack = NULL;
                        break;

                    default:
                        ack = PSTR("AOK\r\n");
                        break;
                }

                if (ack != NULL && !waitResp(ack, wait)) {    // retry from the first
                    enterCommandMode();
                    break;
                }
            }
        }
        if (i == strlen_P(tbl)) {   // success
            break;
        }
    }
}

void RNBTHid::sendCommands(const __FlashStringHelper *cmd, u16 wait) {
    sendCommands((const char *)cmd, wait);
}

void RNBTHid::setMode(pad_mode mode) {
    if (m_isSleep)
        return;

    digitalWrite(PIN_BT_CONNECT, LOW);
    delay(500);

    enterCommandMode();
    sendCommands(TBL_COMMANDS);
    
    if (mode == MODE_JOYSTICK) {
        sendCommands(F("SH,0243\nS-,PiPadJoy\n"));
    } else if (mode == MODE_KEYBOARD_MOUSE) {
        sendCommands(F("SH,0233\nS-,PiPadKM\n"));
    }
    sendCommands(TBL_REBOOT);
    digitalWrite(PIN_BT_CONNECT, HIGH);
    delay(200);
}

void RNBTHid::update(void) {
    if (!m_isSleep && m_isConnected) {
        if (mKeyboard.isUpdated()) {
            mKeyboard.update();
        }
        if (mMouse.isUpdated()) {
            mMouse.update();
        }
        if (mJoystick.isUpdated()) {
            mJoystick.update();
        }
    }
}

void RNBTHid::sleep(bool en, bool force) {
    if ((!m_isSleep || force) && en) {
        enterCommandMode();
        sendCommands(TBL_SLEEP);
        LOG(F("SLEEP\n"));
        digitalWrite(PIN_BT_CONNECT, LOW);
    }

    if ((m_isSleep || force) && !en) {
        enterCommandMode();
        sendCommands(TBL_REBOOT);
        LOG(F("WAKE\n"));
        digitalWrite(PIN_BT_CONNECT, HIGH);
    }
    m_isSleep = en;
}

void RNBTHid::loop(long ts) {
    waitResp((const char*)NULL, 0);
}

/*
*****************************************************************************************
* JOYSTICK
*****************************************************************************************
*/
void RNBTJoystick::setButton(u8 idx, u8 on) {
    if (on) {
        mReport.button |= BV(idx);
    } else {
        mReport.button &= ~BV(idx);
    }
    m_isUpdated = true;
    return true;
}

bool RNBTJoystick::press(u16 k) {
    mReport.button |= k;
    m_isUpdated = true;
    return true;
}

bool RNBTJoystick::release(u16 k) {
    mReport.button &= ~k;
    m_isUpdated = true;
}

void RNBTJoystick::releaseAll(void) {
    mReport.button = 0;
    m_isUpdated = true;
    update();
}

void RNBTJoystick::setXAxis(s8 v) {
    mReport.x = v;
    m_isUpdated = true;
}

void RNBTJoystick::setYAxis(s8 v) {
    mReport.y = v;
    m_isUpdated = true;
}

void RNBTJoystick::setZAxis(s8 v) {
    mReport.z = v;
    m_isUpdated = true;
}

void RNBTJoystick::setRzAxis(s8 v) {
    mReport.rz = v;
    m_isUpdated = true;
}

void RNBTJoystick::update(void) {
    u8 packet[8];

    if (m_isUpdated) {
        packet[0] = 0xFD;
        packet[1] = 6;      // length
        memcpy(&packet[2], &mReport, sizeof(struct reportJoystick));
        
        for (u8 i = 0; i < sizeof(packet); i++) {
            Serial1.write(packet[i]);
        }
        m_isUpdated = false;
//        LOG(F("joystick update\n"));
    }
}


/*
*****************************************************************************************
* MOUSE
*****************************************************************************************
*/
bool RNBTMouse::press(u16 k) {
    mReport.button |= k;
    m_isUpdated = true;
    return true;
}

bool RNBTMouse::release(u16 k) {
    mReport.button &= ~k;
    m_isUpdated = true;
    return true;
}

void RNBTMouse::releaseAll(void) {
    mReport.button = 0;
    m_isUpdated = true;
    update();
}

void RNBTMouse::move(s8 mx, s8 my, s8 wheel) {
    mReport.mx = mx;
    mReport.my = my;
    mReport.wheel = wheel;
    m_isUpdated = true;
}

void RNBTMouse::update(void) {
    u8 packet[7];

    if (m_isUpdated) {
        packet[0] = 0xFD;
        packet[1] = 5;                     // length
        packet[2] = 2;                     // mouse
        memcpy(&packet[3], &mReport, sizeof(struct reportMouse));
        
        for (u8 i = 0; i < sizeof(packet); i++) {
            Serial1.write(packet[i]);
        }
        m_isUpdated = false;
//        LOG(F("%8ld mouse update\n"), millis());
    }
}


/*
*****************************************************************************************
* KEYBOARD
*****************************************************************************************
*/
extern const u8 _asciimap[128] PROGMEM;

bool RNBTKey::press(u16 k) {
	u8 i;
    
	if (k >= 136) {			// it's a non-printing key (not a modifier)
		k = k - 136;
	} else if (k >= 128) {	// it's a modifier key
		mReport.modifiers |= (1<<(k-128));
		k = 0;
	} else {				// it's a printing key
		k = pgm_read_byte(_asciimap + k);
		if (!k) {
			return false;
		}
		if (k & 0x80) {						// it's a capital letter or other character reached with shift
			mReport.modifiers |= 0x02;	// the left shift modifier
			k &= 0x7F;
		}
	}
	
	// Add k to the key report only if it's not already present
	// and if there is an empty slot.
	if (mReport.keys[0] != k && mReport.keys[1] != k && 
		mReport.keys[2] != k && mReport.keys[3] != k &&
		mReport.keys[4] != k && mReport.keys[5] != k) {
		
		for (i = 0; i < 6; i++) {
			if (mReport.keys[i] == 0x00) {
				mReport.keys[i] = k;
				break;
			}
		}
		if (i == 6) {
			return false;
		}	
	}
    m_isUpdated = true;
    
	return true;
}

// release() takes the specified key out of the persistent key report and
// sends the report.  This tells the OS the key is no longer pressed and that
// it shouldn't be repeated any more.
bool RNBTKey::release(u16 k) {
	u8 i;
    
	if (k >= 136) {			// it's a non-printing key (not a modifier)
		k = k - 136;
	} else if (k >= 128) {	// it's a modifier key
		mReport.modifiers &= ~(1<<(k-128));
		k = 0;
	} else {				// it's a printing key
		k = pgm_read_byte(_asciimap + k);
		if (!k) {
			return false;
		}
		if (k & 0x80) {							// it's a capital letter or other character reached with shift
			mReport.modifiers &= ~(0x02);	// the left shift modifier
			k &= 0x7F;
		}
	}
	
	// Test the key report to see if k is present.  Clear it if it exists.
	// Check all positions in case the key is present more than once (which it shouldn't be)
	for (i = 0; i < 6; i++) {
		if (0 != k && mReport.keys[i] == k) {
			mReport.keys[i] = 0x00;
		}
	}
    m_isUpdated = true;
    
	return true;
}

void RNBTKey::releaseAll(void) {
	mReport.keys[0] = 0;
	mReport.keys[1] = 0;	
	mReport.keys[2] = 0;
	mReport.keys[3] = 0;	
	mReport.keys[4] = 0;
	mReport.keys[5] = 0;	
	mReport.modifiers = 0;
    m_isUpdated = true;
    update();
}

void RNBTKey::update(void) {
    u8 packet[11];

    if (m_isUpdated) {
        packet[0] = 0xFD;
        packet[1] = 9;                     // length
        packet[2] = 1;                     // key
        memcpy(&packet[3], &mReport, sizeof(struct reportKey));
        for (u8 i = 0; i < sizeof(packet); i++) {
            Serial1.write(packet[i]);
        }
/*
        for (u8 i = 0; i < 6; i++) {
            LOG(F("key = %d:%02x\n"), i, mReport.keys[i]);
        }
        LOG(F("========\n"));
*/        

//        LOG(F("keyboard update\n"));

        m_isUpdated = false;
    }
}


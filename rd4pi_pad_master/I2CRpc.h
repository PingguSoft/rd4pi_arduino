#ifndef _I2CRPC_H_
#define _I2CRPC_H_

#include "common.h"
#include "config.h"
#include "src/SoftI2CMaster/SoftWire.h"
#undef Wire


/*
*****************************************************************************************
* CONSTANTS
*****************************************************************************************
*/
enum {
    I2C_REG_NOP = 0x00,
    I2C_REG_SET_LED_RGB,
    I2C_REG_SET_LED_BRIGHTNESS,
    I2C_REG_GET_EVENTS,
};


/*
*****************************************************************************************
* CLASS
*****************************************************************************************
*/
class I2CRpc {
public:
    virtual void setLedRGB(u8 index, u8 r, u8 g, u8 b, u16 blink) = 0;
    virtual void setLedBrightness(u8 index, u8 brightness) = 0;
    virtual bool getEvents(u16 *btn, u16 *x, u16 *y, s8 *mx, s8 *my, s8 *mz) = 0;

private:
};

class I2CRpcClient : public I2CRpc {
public:
    I2CRpcClient();
    void setLed(u8 index, u32 rgb, u16 blink);
    void setLedRGB(u8 index, u8 r, u8 g, u8 b, u16 blink);
    void setLedHSV(u8 index, u16 h, u8 s, u8 v, u16 blink);
    void setLedBrightness(u8 index, u8 brightness);
    u32  getLed(u8 index) { return mLeds[index]; }
    u16  getLedBlink(u8 index) { return mBlinks[index]; }


    bool getEvents(u16 *btn, u16 *x, u16 *y, s8 *mx, s8 *my, s8 *mz);
    
private:
    void setLed(u8 cmd, u8 index, u8 a1, u8 a2, u8 a3, u16 blink);

    SoftWire    mSoftI2C;
    u32         mLeds[NUM_LEDS];
    u16         mBlinks[NUM_LEDS];
    u8          mBrightness[NUM_LEDS];
};

#endif

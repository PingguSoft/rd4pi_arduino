#ifndef _EMUL_MCP23017_H_
#define _EMUL_MCP23017_H_
#include "common.h"
#include "config.h"

/*
*****************************************************************************************
* CONSTANTS
*****************************************************************************************
*/


/*
*****************************************************************************************
* CLASS
*****************************************************************************************
*/
class EmulMCP23017 {
public:
    EmulMCP23017();
    void setup(void);
    void receive(int len);
    void request(void);
    void process(struct settings *pSetting, u32 btns, u16 lx, u16 ly, u16 rx, u16 ry, s8 mx, s8 my);

private:
    void processAxis(u16 val, u16 mid, u8 idxLittle, u8 idxBig, u16 *mask);
        
    u8   m_ucReg;
    u16  m_wPUMask;
    u16  m_wGpio;
};

#endif


#include <Arduino.h>
#include <Wire.h>
#include "utils.h"
#include "EmulMCP23017.h"


/*
*****************************************************************************************
* CONST
*****************************************************************************************
*/
#define MPC23017_GPIOA_MODE	        0x00
#define MPC23017_GPIOB_MODE	        0x01
#define MPC23017_GPIOA_PULLUPS_MODE 0x0c
#define MPC23017_GPIOB_PULLUPS_MODE 0x0d
#define MPC23017_GPIOA_READ         0x12
#define MPC23017_GPIOB_READ         0x13

enum {
    BIT_MCP23017_UP = 0,
    BIT_MCP23017_DOWN,
    BIT_MCP23017_LEFT,
    BIT_MCP23017_RIGHT,
    BIT_MCP23017_START,
    BIT_MCP23017_SELECT,
    BIT_MCP23017_A,
    BIT_MCP23017_B,
    BIT_MCP23017_TR,
    BIT_MCP23017_Y,
    BIT_MCP23017_X,
    BIT_MCP23017_TL,
    BIT_MCP23017_C,
    BIT_MCP23017_TR2,
    BIT_MCP23017_Z,
    BIT_MCP23017_TL2
};

enum {
    BIT_MCP23017_PU_A = 0,
    BIT_MCP23017_PU_B = 1
};

/*
*****************************************************************************************
* CONST TABLE
*****************************************************************************************
*/
static const u8 PROGMEM TBL_GPIO_MAP[] = {
    BIT_HAT_UP, BIT_HAT_DOWN, BIT_HAT_LEFT, BIT_HAT_RIGHT,
    BIT_START, BIT_SELECT, BIT_A, BIT_B, 

    BIT_R1, BIT_Y, BIT_X, BIT_L1,
    BIT_LTHUMB, BIT_R2, BIT_RTHUMB, BIT_L2
};

/*
*****************************************************************************************
* VARIABLES
*****************************************************************************************
*/
static EmulMCP23017 *instance = NULL;


/*
*****************************************************************************************
* Functions
*****************************************************************************************
*/

void receiveEvent(int len) {
    instance->receive(len);
}

void requestEvent(void) {
    instance->request();
}

EmulMCP23017::EmulMCP23017() {
    instance  = this;
    m_wGpio   = 0;
    m_wPUMask = 0xffff;
}

void EmulMCP23017::setup(void) {
    // i2c slave
    Wire.begin(I2C_SLAVE_ADDR_MICRO);
    Wire.onReceive(receiveEvent);
    Wire.onRequest(requestEvent);    
}

/*
*****************************************************************************************
* I2C slave events
*****************************************************************************************
*/
void EmulMCP23017::receive(int len) {
    m_ucReg = Wire.read();

    switch (m_ucReg) {
        case MPC23017_GPIOA_PULLUPS_MODE:
            m_wPUMask |= Wire.read();
            break;

        case MPC23017_GPIOB_PULLUPS_MODE:
            m_wPUMask |= ((u16)Wire.read() << 8);
            break;
    }
//    LOG(F("reg : %x len:%d\n"), m_ucReg, len);
}

void EmulMCP23017::request(void) {
    u8  packet;
    u32 axis;

    switch (m_ucReg) {
        case MPC23017_GPIOA_READ:
            packet = m_wGpio & 0xff;
            Wire.write(&packet, 1);
            break;

        case MPC23017_GPIOB_READ:
            packet = (m_wGpio >> 8) & 0xff;
            Wire.write(&packet, 1);
            break;
    }
    m_ucReg = 0xff;
}

void EmulMCP23017::processAxis(u16 val, u16 mid, u8 idxLittle, u8 idxBig, u16 *mask) {
    u16 maskLit = (u16)BV(idxLittle - BIT_MCP23017_UP);
    u16 maskBig = (u16)BV(idxBig    - BIT_MCP23017_UP);
    u16 thr     = mid * 3 / 10;
    
    if (val < mid - thr) {
        if (*mask & maskBig) {
            *mask = *mask & ~maskBig;
        }
        *mask = *mask | maskLit;
    } else if (val > mid + thr) {
        if (*mask & maskLit) {
            *mask = *mask & ~maskLit;
        }
        *mask = *mask | maskBig;
    } else {
        if (*mask & maskLit) {
            *mask = *mask & ~maskLit;
        }
        if (*mask & maskBig) {
            *mask = *mask & ~maskBig;
        }
    }
}

void EmulMCP23017::process(struct settings *pSetting, u32 btns, u16 lx, u16 ly, u16 rx, u16 ry, s8 mx, s8 my) {
    u16 mid;
    u16 gpio = 0;
    
    mid = (pSetting->left.minX + pSetting->left.maxX) >> 1;
    processAxis(lx, mid, BIT_MCP23017_UP, BIT_MCP23017_DOWN, &gpio);

    mid = (pSetting->left.minY + pSetting->left.maxY) >> 1;
    processAxis(ly, mid, BIT_MCP23017_LEFT, BIT_MCP23017_RIGHT, &gpio);

    for (u8 i = 0; i < sizeof(TBL_GPIO_MAP); i++) {
        u8 idx = pgm_read_byte(&TBL_GPIO_MAP[i]);
        
        if (btns & BV(idx)) {
            gpio |= BV(i);
        }
    }

    gpio = gpio ^ m_wPUMask;

    if (gpio != m_wGpio) {
//        LOG(F("reg:%04x\n"), gpio);
        m_wGpio = gpio;
    }
}

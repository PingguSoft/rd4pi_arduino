#ifndef _RN_BT_HID_H_
#define _RN_BT_HID_H_
#include "common.h"
#include "config.h"

/*
*****************************************************************************************
* CONSTANTS
*****************************************************************************************
*/

struct reportKey {
    u8  modifiers;
    u8  reserved;
    u8  keys[6];
};

struct reportMouse {
    u8  button;
    s8  mx;
    s8  my;
    s8  wheel;
};

struct reportJoystick {
    s8  x;
    s8  y;
    s8  z;
    s8  rz;
    u16 button;
};

/*
*****************************************************************************************
* CLASS
*****************************************************************************************
*/
class LiteHid {
public:
    LiteHid()   { m_isUpdated = false; }
    virtual bool press(u16 k)     = 0;
    virtual bool release(u16 k)   = 0;
    virtual void releaseAll(void) = 0;
    virtual void update(void)     = 0;

    bool isUpdated(void)    { return m_isUpdated; }

protected:
    bool m_isUpdated;
};

class RNBTKey : public LiteHid {
public:
    RNBTKey()       { memset(&mReport, 0, sizeof(mReport)); }
    
    bool press(u16 k);
    bool release(u16 k);
    void releaseAll(void);
    void update(void);

private:
    struct reportKey mReport;
};

class RNBTMouse : public LiteHid {
public:
    RNBTMouse()     { memset(&mReport, 0, sizeof(mReport)); }    
    
    bool press(u16 k);
    bool release(u16 k);
    void releaseAll(void);
    void update(void);

    void move(s8 mx, s8 my, s8 wheel);
private:
    struct reportMouse mReport;
};

class RNBTJoystick : public LiteHid {
public:
    RNBTJoystick()  { memset(&mReport, 0, sizeof(mReport)); }
    
    bool press(u16 k);
    bool release(u16 k);
    void releaseAll(void);
    void update(void);

    void setButton(u8 idx, u8 on);
    void setXAxis(s8 v);
    void setYAxis(s8 v);
    void setZAxis(s8 v);
    void setRzAxis(s8 v);
private:
    struct reportJoystick mReport;
};

class RNBTHid {
public:
    RNBTHid();
    void setup(void);
    void setMode(pad_mode mode);
    void loop(long ts);
    void update(void);
    void sleep(bool en, bool force = false);
    bool isSleep(void)      { return m_isSleep; }
    bool isConnected(void)  { return m_isConnected; }
    
    RNBTKey         mKeyboard;
    RNBTMouse       mMouse;
    RNBTJoystick    mJoystick;


private:
    u8   waitRx(u8 cnt, u16 wait = 100, bool isEmpty = false);
    void enterCommandMode(void);
    bool waitResp(const char *resp, u16 wait = 100);
    bool waitResp(const __FlashStringHelper *resp, u16 wait = 100);
    void sendCommands(const char *tbl, u16 wait = 100);
    void sendCommands(const __FlashStringHelper *cmd, u16 wait = 100);

    bool            m_isSleep;
    bool            m_isConnected;
    bool            m_isCommandMode;
};

#endif

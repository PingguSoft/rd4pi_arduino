#include <Arduino.h>
#include <Adafruit_NeoPixel.h>
#include "utils.h"
#include "StatusLed.h"
#include "I2CRpc.h"


/*
*****************************************************************************************
* CONSTANTS
*****************************************************************************************
*/


/*
*****************************************************************************************
* VARIABLES
*****************************************************************************************
*/


/*
*****************************************************************************************
* Functions
*****************************************************************************************
*/
I2CRpcClient::I2CRpcClient() {
    mSoftI2C = SoftWire();
    mSoftI2C.begin();

    for (u8 i = 0; i < NUM_LEDS; i++) {
        mBrightness[i] = 255;
    }
}

void I2CRpcClient::setLed(u8 cmd, u8 index, u8 a1, u8 a2, u8 a3, u16 blink) {
    mSoftI2C.beginTransmission(I2C_SLAVE_ADDR_MINI);
    mSoftI2C.write(cmd);
    mSoftI2C.write(index);
    mLeds[index] = ((u32)a1 << 16) | ((u32)a2 << 8) | a3;

    a1 = ((u16)a1 * mBrightness[index]) >> 8;
    a2 = ((u16)a2 * mBrightness[index]) >> 8;
    a3 = ((u16)a3 * mBrightness[index]) >> 8;
    
    mSoftI2C.write(a1);
    mSoftI2C.write(a2);
    mSoftI2C.write(a3);
    mSoftI2C.write((u8)(blink >> 8));
    mSoftI2C.write((u8)(blink & 0xff));
    mSoftI2C.endTransmission();
    mBlinks[index] = blink;
}

void I2CRpcClient::setLed(u8 index, u32 rgb, u16 blink) {
    setLed((u8)I2C_REG_SET_LED_RGB, index, (rgb >> 16) & 0xff, (rgb >> 8) & 0xff, rgb & 0xff, blink);
}

void I2CRpcClient::setLedRGB(u8 index, u8 r, u8 g, u8 b, u16 blink) {
    setLed((u8)I2C_REG_SET_LED_RGB, index, r, g, b, blink);
}

void I2CRpcClient::setLedHSV(u8 index, u16 h, u8 s, u8 v, u16 blink) {
    u32 rgb = Adafruit_NeoPixel::gamma32(Adafruit_NeoPixel::ColorHSV(HUE(h), SAT(s), VAL(v)));
    setLed((u8)I2C_REG_SET_LED_RGB, index, (rgb >> 16) & 0xff, (rgb >> 8) & 0xff, rgb & 0xff, blink);
}

void I2CRpcClient::setLedBrightness(u8 index, u8 brightness) {
    mBrightness[index] = brightness;
//    setLed(index, getLed(index), getLedBlink(index));
}

bool I2CRpcClient::getEvents(u16 *btn, u16 *x, u16 *y, s8 *mx, s8 *my, s8 *mz) {
    u8   rx = mSoftI2C.requestFrom(I2C_SLAVE_ADDR_MINI, 9, (u8)I2C_REG_GET_EVENTS, 1, 1);
    
    if (rx > 0) {
        u8   buf[rx];
        u32  axis;

        //LOG(F("RX:"));
        for (u8 i = 0; i < rx; i++) {
            buf[i] = mSoftI2C.read();
            //LOG(F("%02x, "), buf[i]);
        }
        //LOG(F("\n"));
        if (buf[0] == 0xCA) {
            *btn   = Utils::get16(&buf[1]);
            axis   = ((u32)buf[5] << 16);
            axis  |= ((u32)buf[4] << 8);
            axis  |= ((u32)buf[3] << 0);
            
            *x  = axis & 0x0fff;
            *y  = (axis >> 12) & 0x0fff;
            *mx = (s8)buf[6];
            *my = (s8)buf[7];
            *mz = (s8)buf[8];

            return true;
        }
    }

    return false;
}

